Ecoomerce app

- Ecommerce: Virtual env
- src: source code
- static_cdn: Carpeta que contiene imagenes solo de prueba

Para empezar deben hacer todo esto desde la carpeta donde se va a guardar
el repo.

Clonar el repositorio y instalar los siguientes requisitos:

Para iniciar:
- Instalar python 3.6
- Instalar django 1.11

pip3.6 install Django==1.11 		# pip3 install django==1.11

Una vez instalados, crear e iniciar el virtualenv:

- pip3 install virtualenv
- source eCommerce/bin/activate  	# Apple
- eCommerce\Scripts\activate		# Windows

Para desactivar el VE:
- deactivate						# desactivar el ve

Instalar Pillow dentro del VE (tal vez no sea necesario):

- pip3 install pillow  				# para manejar imagenes

Con los requisitos instalados tienen que correr lo siguiente:

- python3 manage.py makemigrations
- python3 manage.py migrate
- python3 manage.py collectstatic  	# para manejar el css, las imagenes y files

Crear un usuario admin:

- python3 manage.py createsuperuser

Correr el server:

- python3 manage.py runserver

Entrar al admin.







