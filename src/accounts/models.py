# coding=utf-8
from django.db import models


class GuestEmail(models.Model):
    """
    Modelo para uso de checkout usando un guest email
    """
    email       = models.EmailField()
    active      = models.BooleanField(default=True)
    updated     = models.DateTimeField(auto_now=True)
    timestamp   = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.email
