# coding=utf-8
from django.contrib import admin

from .models import GuestEmail


admin.site.register(GuestEmail)
