# coding=utf-8
from django import forms
from django.contrib.auth import get_user_model

User = get_user_model()


class GuestForm(forms.Form):
    """
    Crea un form para que el usuario ingrese como guest
    usando un correo electronico
    """
    email       = forms.EmailField()


class LoginForm(forms.Form):
    """
    Crea un form para que el usuario pueda ingresar con nombre de usuario
    """
    username    = forms.CharField()
    password    = forms.CharField(label='Contraseña', widget=forms.PasswordInput)


class RegisterForm(forms.Form):
    """
    Permite que el usuario se registre
    ( Falta por terminar )
    """
    username    = forms.CharField(label='Nombre de usuario')
    email       = forms.EmailField()
    password    = forms.CharField(label='Contraseña', widget=forms.PasswordInput)
    password2   = forms.CharField(label='Confirmar contraseña', widget=forms.PasswordInput)

    def clean_username(self):
        """
        Revisa si el nombre de usuario ya existe
        :return: nombre de usuario unico
        """
        username    = self.cleaned_data.get('username')
        qs          = User.objects.filter(username=username)
        if qs.exists():
            raise forms.ValidationError("Nombre de usario ya en uso")
        return username

    def clean_email(self):
        """
        Revisa si el email ya existe
        :return: email unico
        """
        email   = self.cleaned_data.get('email')
        qs      = User.objects.filter(email=email)
        if qs.exists():
            raise forms.ValidationError("Email ya en uso")
        return email

    def clean(self):
        """
        Revisa si la contraseña es valida
        :return: contraseña valida
        """
        data        = self.cleaned_data
        password    = self.cleaned_data.get('password')
        password2   = self.cleaned_data.get('password2')
        if password2 != password:
            raise forms.ValidationError('Contraseñas deben coincidir')
        return data
