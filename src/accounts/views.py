# coding=utf-8
from django.shortcuts import render, redirect
from django.contrib.auth import get_user_model, authenticate, login
from django.utils.http import is_safe_url

from .forms import LoginForm, RegisterForm, GuestForm
from .models import GuestEmail


User = get_user_model()


def guest_register_page(request):
    """
    Pagina para registrar al usuario usando un guest email
    :param request: request
    :return: registrado como guest, retorna a la carreta, sino a register
    """
    form = GuestForm(request.POST or None)
    next_ = request.GET.get('next')
    next_post = request.POST.get('next')
    redirect_path = next_ or next_post or None
    if form.is_valid():
        email    = form.cleaned_data.get('email')
        new_guest_email = GuestEmail.objects.create(email=email)
        request.session['guest_email_id'] = new_guest_email.id

        if is_safe_url(redirect_path, request.get_host()):
            return redirect(redirect_path)
        else:
            return redirect("/register/")

    return redirect("/register/")


def login_page(request):
    """
    Durante el checkout el usuario puede entrar con su username si ya
    existe en el sistema.
    :param request: request
    :return: pagina de login
    """
    form = LoginForm(request.POST or None)
    context = {
        'form': form,
    }
    next_ = request.GET.get('next')
    next_post = request.POST.get('next')
    redirect_path = next_ or next_post or None
    if form.is_valid():
        username = form.cleaned_data.get('username')
        password = form.cleaned_data.get('password')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            try:
                del request.session['guest_email_id']  # elimina el email de guest si fue ingresado
            except:
                pass
            if is_safe_url(redirect_path, request.get_host()):
                return redirect(redirect_path)  # se asegura que el url sea seguro
            else:
                return redirect("/")
        else:
            print("Error")
    return render(request, 'accounts/login.html', context)


def register_page(request):
    """
    registra a un usuario con username, email, y contraseña
    :param request: request
    :return: usuario creado
    """
    form = RegisterForm(request.POST or None)
    context = {
        'form': form,
    }
    if form.is_valid():
        username = form.cleaned_data.get('username')
        email = form.cleaned_data.get('email')
        password = form.cleaned_data.get('password')
        User.objects.create_user(username, email, password)
        return redirect('/')

    return render(request, 'accounts/register.html', context)
