# coding=utf-8
from django import forms

from .models import Address


class AddressForm(forms.ModelForm):
    class Meta:
        model = Address
        fields = [
            'direccion_linea_1',
            'direccion_linea_2',
            'ciudad',
            'provincia'
        ]
