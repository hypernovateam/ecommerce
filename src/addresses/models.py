# coding=utf-8
from django.db import models

from billing.models import BillingProfile


# Tipos de envio: de entrega, y de pago
# Lado izquierdo es visto por el usuario, lado derecho es para el database
ADDRESS_TYPES = (
    ('pago', 'Pago'),
    ('entrega', 'Entrega'),
)


class Address(models.Model):
    billing_profile         = models.ForeignKey(BillingProfile)
    address_type            = models.CharField(max_length=120, choices=ADDRESS_TYPES)
    direccion_linea_1       = models.CharField(max_length=120)
    direccion_linea_2       = models.CharField(max_length=120, blank=True, null=True)
    ciudad                  = models.CharField(max_length=120)
    provincia               = models.CharField(max_length=120)

    def __str__(self):
        return str(self.billing_profile)

    def get_address(self):
        return "{line1}\n{line2}\n{city}, {province}".format(
            line1=self.direccion_linea_1,
            line2=self.direccion_linea_2 or "",
            city=self.ciudad,
            province=self.provincia
        )
