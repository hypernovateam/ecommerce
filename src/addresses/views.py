# coding=utf-8
from django.shortcuts import redirect
from django.utils.http import is_safe_url

from .forms import AddressForm
from .models import Address
from billing.models import BillingProfile


def checkout_address_create_view(request):
    """
    Crea una vista para que el usuario pueda elegir su direccion
    sea para entrega o direccion de pago (actual)
    :param request:
    :return:
    """
    form = AddressForm(request.POST or None)

    next_ = request.GET.get('next')
    next_post = request.POST.get('next')
    redirect_path = next_ or next_post or None
    if form.is_valid():
        instance = form.save(commit=False)  # antes de guardar revisa si ya existe
        billing_profile, billing_profile_created = BillingProfile.objects.new_or_get(request)

        if billing_profile is not None:  # si no existe la crea
            address_type = request.POST.get('address_type', 'entrega')
            instance.billing_profile = billing_profile
            instance.address_type = address_type
            instance.save()
            request.session[address_type + "_address_id"] = instance.id

        else:
            return redirect("cart:checkout")  # si existe regresa a la carreta

        if is_safe_url(redirect_path, request.get_host()):
            return redirect(redirect_path)

    return redirect("cart:checkout")


def checkout_address_reuse_view(request):
    """
    Si existen direcciones ya usadas por el usuario en la base de datos
    permite que el usuario la pueda escoger una vez entrado de nuevo
    :param request: request
    :return: pagina de checkout
    """
    if request.user.is_authenticated():
        next_ = request.GET.get('next')
        next_post = request.POST.get('next')
        redirect_path = next_ or next_post or None
        if request.method == 'POST':
            entrega_address = request.POST.get('entrega_address', None)
            address_type = request.POST.get('address_type', 'entrega')
            billing_profile, billing_profile_created = BillingProfile.objects.new_or_get(request)
            if entrega_address is not None:
                qs = Address.objects.filter(billing_profile=billing_profile, id=entrega_address)
                if qs.exists():
                    request.session[address_type + "_address_id"] = entrega_address

                if is_safe_url(redirect_path, request.get_host()):
                    return redirect(redirect_path)

    return redirect("cart:checkout")
