# coding=utf-8
from django.apps import AppConfig


class TagsConfig(AppConfig):
    name = 'tags'
