# coding=utf-8
from django.views.generic import ListView, DetailView
from django.http import Http404

from carts.models import Cart

from .models import Product


class ProductFeaturedListView(ListView):
    """
    Usa las funciones basicas de django ListView
    """
    template_name = "products/list.html"

    def get_queryset(self, *args, **kwargs):
        """
        Obtiene el query que contiene los datos pedidos por el usuario
        :param args: datos
        :param kwargs: datos clave
        :return: productos que tengan feature activado (True)
        """
        request = self.request
        return Product.objects.all().featured()  # Comes from models Manager


class ProductFeaturedDetailView(DetailView):
    """
    Clase que muestra productos en promocion (featured == True)
    """
    queryset = Product.objects.all().featured()  # Comes from models Manager
    template_name = "products/featured-detail.html"


class ProductListView(ListView):
    template_name = "products/list.html"

    def get_context_data(self, *args, **kwargs):
        context = super(ProductListView, self).get_context_data(*args, **kwargs)
        cart_obj, new_obj = Cart.objects.new_or_get(self.request)
        context['cart'] = cart_obj
        return context

    def get_queryset(self, *args, **kwargs):
        """
        Funcion en clase de vista que muestra todos los productos
        :param args: datos
        :param kwargs: datos clave
        :return: todos los productos activos
        """
        request = self.request
        return Product.objects.all()


class ProductDetailSlugView(DetailView):
    """
    Clase que muestra los productos si son buscados
    por su nombre clave.
    """
    queryset = Product.objects.all()
    template_name = "products/detail.html"

    def get_context_data(self, *args, **kwargs):
        context = super(ProductDetailSlugView, self).get_context_data(*args, **kwargs)
        cart_obj, new_obj = Cart.objects.new_or_get(self.request)
        context['cart'] = cart_obj
        return context


    def get_object(self, *args, **kwargs):
        """
        Funcion que muestra el producto llamado por su nombre clave
        :param args: datos
        :param kwargs: datos clave
        :return: objeto si existe y activo en forma detallada, sino 404
        """
        request = self.request
        slug = self.kwargs.get('slug')
        try:
            instance = Product.objects.get(slug=slug, active=True)
        except Product.DoesNotExist:
            raise Http404("Not Found..")
        except Product.MultipleObjectsReturned:
            qs = Product.objects.filter(slug=slug, active=True)
            instance = qs.first()
        except:
            raise Http404("MMMmmm")
        return instance


class ProductDetailView(DetailView):
    """
    Muestra el producto detallado
    """
    template_name = "products/detail.html"

    def get_queryset(self, *args, **kwargs):
        """
        Obtiene el producto buscando por su primary key
        :param args: datos
        :param kwargs: datos clave
        :return: el producto pedido por el usuario
        """
        request = self.request
        pk = self.kwargs.get('pk')
        return Product.objects.filter(pk=pk)
