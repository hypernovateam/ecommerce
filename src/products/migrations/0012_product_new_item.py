# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2019-02-24 19:24
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0011_product_timestamp'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='new_item',
            field=models.BooleanField(default=True),
        ),
    ]
