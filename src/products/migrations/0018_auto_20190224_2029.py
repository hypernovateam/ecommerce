# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2019-02-24 20:29
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0017_product_discounte'),
    ]

    operations = [
        migrations.RenameField(
            model_name='product',
            old_name='discprice',
            new_name='discount',
        ),
        migrations.RenameField(
            model_name='product',
            old_name='discounte',
            new_name='indiscount',
        ),
    ]
