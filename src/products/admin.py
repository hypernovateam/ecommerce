# coding=utf-8
from django.contrib import admin

from .models import Product


class ProductAdmin(admin.ModelAdmin):
    """
    Vista para el admin de django.
    Cambia la vista para que muestre el nombre mas el 'slug'
    """
    list_display = ['__str__', 'slug']  # admin view

    class Meta:
        """
        El modelo Meta es importado desde Product
        """
        model = Product


admin.site.register(Product, ProductAdmin)
