# coding=utf-8
from random import randint
import os
from django.db import models
from django.db.models import Q
from django.db.models.signals import pre_save  # before being save into the database
from ecommerce.utils import unique_slug_generator  # usado para generar nombres clave
from django.urls import reverse


def get_filename_ext(filepath):
    """
    Funcion usada para obtener la extension de la imagen
    :param filepath: imagen ingresada por el usuario
    :return: direccion guardada en el servidor para su uso
    """
    basename = os.path.basename(filepath)
    name, ext = os.path.splitext(basename)
    return name, ext


def upload_image_path(instance, filename):
    """
    Funcion que se usa para subir imagenes.
    Cambia el nombre a uno usado por el servidor
    Solo es para prueba, no para lanzamiento.
    :param instance: los datos siendo usados en el momento
    :param filename: el nombre de la imagen que va a ser subida al servidor
    :return: la direccion de la imagen en el servidor
    """
    new_filename = randint(1, 10000)
    name, ext = get_filename_ext(filename)
    final_filename = f'{new_filename}{ext}'  # f' solo se usa para django 1.11
    return f'products/{new_filename}/{final_filename}'


class ProductQuerySet(models.query.QuerySet):  # function of the ProductManager
    def featured(self):
        """
        Revisa si el producto esta en promocion
        :return: productos en promocion
        """
        return self.filter(feature=True)

    def active(self):
        """
        Revisa si el producto esta activo
        :return: productos activos
        """
        return self.filter(active=True)

    def newItems(self):
        return self.filter(new_item=True)

    def discounte(self):
        return self.filter(indiscount=True)

    def search(self, query):
        lookups = (
                Q(title__icontains=query) |
                Q(description__icontains=query) |
                Q(tag__title__icontains=query))
        return self.filter(lookups).distinct()


class ProductManager(models.Manager):  # function of objects
    """
    El manager de los objetos
    Sirve para modificar lo que se puede mostrar al usuario
    """

    def get_queryset(self):
        """
        Obtiene el query especifico pedido del producto
        :return: funcion modificada de vista por usuario
        """
        return ProductQuerySet(self.model, using=self._db)

    def all(self):
        """
        Funcion que retorna todos los elementos en la base de datos
        :return: datos que esten activos (active == True)
        """
        return self.get_queryset().active()

    def features(self):
        """
        Funcion creada para mostrar si el producto esta en promocion
        :return: objetos que esten en promocion y activos
        """
        return self.get_queryset().featured()

    def new_item(self):
        return self.get_queryset().newItems()

    def discount_items(self):
        return self.get_queryset().discounte()

    def get_by_id(self, id):
        """
        BUsca objetos por el numero de id en la base de datos
        :param id: el numero de identificacion pedido
        :return: producto relacionado con el id si existe
        """
        qs = self.get_queryset().filter(id=id)  # Product.objects
        if qs.count() == 1:
            return qs.first()
        return None

    def search(self, query):
        return self.get_queryset().active().search(query)


DISCOUNT_CHOICES = (
    (15, 15),
    (20, 20),
    (25, 25),
    (30, 30),
)


class Product(models.Model):
    """
    Product es la arquitectura de los productos.
    Como pueden ser usados mas las caracteristicas.
    Solo pueden ser usados por los admins de la pagina.

    blank == True: si se puede dejar vacio
    unique == True: solo puede haber uno con ese nombre
    null == True: no es necesario ingresar algo para crear el producto

    """
    title           = models.CharField(max_length=120)  # nombre del producto
    slug            = models.SlugField(blank=True, unique=True)  # nombre clave de busqueda
    category        = models.CharField(max_length=120, blank=True, null=True)
    description     = models.TextField()  # descripcion del producto
    price           = models.DecimalField(decimal_places=2, max_digits=10, default=0.00)  # precio inicial
    image           = models.ImageField(upload_to=upload_image_path, null=True, blank=True)  # imagen del producto en nuestro servidor
    feature         = models.BooleanField(default=False)  # si el producto esta de promocion (puede ser basado por usuario)
    new_item        = models.BooleanField(default=True)  # producto nuevo
    indiscount      = models.BooleanField(default=False)    # producto en descuento si o no
    discount        = models.IntegerField(default=0, blank=True, null=True, choices=DISCOUNT_CHOICES)  # porcentaje de descuento
    discprice       = models.DecimalField(decimal_places=2, max_digits=10, default=0.00)
    active          = models.BooleanField(default=True)  # si el producto esta activo
    timestamp       = models.DateTimeField(auto_now_add=True)  # tiempo

    # funcion de productos modificada
    objects = ProductManager()

    # usado por el html
    def get_absolute_url(self):  # HTML template for list
        return reverse("products:detail", kwargs={'slug': self.slug})

    # como va a aparecer en el admin
    def __str__(self):
        """
        Shows the actual name 'title' on the database
        :return: the name of the product
        """
        return self.title.title()

    def discount_price(self):
        original_price = self.price
        discount = self.discount
        disc_total = float(original_price) * (discount/100)
        disc_price = float(original_price) - disc_total
        new_disc_price = format(disc_price, '.2f')
        self.discprice = float(new_disc_price)
        return self.discprice


def product_pre_save_receiver(sender, instance, *args, **kwargs):
    """
    Revisa si tiene nombre clave antes de ser guardado
    Si no tiene, genera un nombre clave que puede ser usado en el html
    :param sender:
    :param instance: los datos siendo usados en el momento
    :param args: datos
    :param kwargs: datos clave
    """
    if not instance.slug:
        instance.slug = unique_slug_generator(instance)  # generates a unique slug for item


pre_save.connect(product_pre_save_receiver, sender=Product)


def discount_price_pre_save(sender, instance, *args, **kwargs):
    if instance.discount:
        instance.discount_price()


pre_save.connect(discount_price_pre_save, sender=Product)
