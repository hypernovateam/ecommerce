# coding=utf-8
import math
from django.db import models
from django.db.models.signals import pre_save, post_save
from ecommerce.utils import unique_order_id_generator

from addresses.models import Address
from billing.models import BillingProfile
from carts.models import Cart

# database en la izquierda, displayed en la derecha
ORDER_STATUS_CHOICES = (
                ('creado', 'Creado'),
                ('pagado', 'Pagado'),
                ('enviado', 'Enviado'),
                ('devuelto', 'Devuelto'),
)


class OrderManager(models.Manager):
    def new_or_get(self, billing_profile, cart_obj):
        """
        Revisa si ya existe la orden
        :param billing_profile: el profile del usuario si esta login
        :param cart_obj: el objeto de la carreta
        :return: objeto y creado
        """
        created = False
        qs = self.get_queryset().filter(
            billing_profile=billing_profile,
            cart=cart_obj,
            active=True,
            status='creado'
        )
        if qs.count() == 1:
            obj = qs.first()
        else:
            obj = self.model.objects.create(
                billing_profile=billing_profile,
                cart=cart_obj)
            created = True
        return obj, created


class Order(models.Model):
    order_id            = models.CharField(max_length=120, blank=True)
    billing_profile     = models.ForeignKey(BillingProfile, null=True, blank=True)
    shipping_address    = models.ForeignKey(Address, related_name="shipping_address", null=True, blank=True)
    billing_address     = models.ForeignKey(Address, related_name="billing_address", null=True, blank=True)
    cart                = models.ForeignKey(Cart)
    status              = models.CharField(max_length=120, default='creado', choices=ORDER_STATUS_CHOICES)
    shipping_total      = models.DecimalField(default=5.99, decimal_places=2, max_digits=10)
    order_total         = models.DecimalField(default=0.00, decimal_places=2, max_digits=10)
    active              = models.BooleanField(default=True)

    objects = OrderManager()  # extiende la opcion de objetos

    def __str__(self):
        return self.order_id

    def update_total(self):
        """
        suma todos los valores en total incluyendo shipping
        :return: nuevo total
        """
        total = math.fsum([self.cart.total,self.shipping_total])
        new_total = format(total, '.2f')
        self.order_total = new_total
        self.save()
        return new_total

    def check_done(self):
        """
        Revisa si para completar la orden ya tiene todos
        los parametros completos
        :return: boolean
        """
        billing_profile = self.billing_profile
        shipping_address = self.shipping_address
        billing_address = self.billing_address
        total = self.order_total
        if billing_profile and billing_address and shipping_address and total > 0:
            return True
        return False

    def mark_paid(self):
        """
        Si check_done es True, el status pasa a
        ser pagado.
        :return: status de la orden
        """
        if self.check_done():
            self.status = "pagado"
            self.save()
        return self.status


def pre_save_create_order_id(sender, instance, *args, **kwargs):
    """
    Crea un numero de orden unica
    :param sender: sender
    :param instance: instance
    :param args: args
    :param kwargs: kwargs
    """
    if not instance.order_id:
        instance.order_id = unique_order_id_generator(instance)
    qs = Order.objects.filter(cart=instance.cart).exclude(billing_profile=instance.billing_profile)
    if qs.exists():
        qs.update(active=False)


pre_save.connect(pre_save_create_order_id, sender=Order)


def post_save_cart_total(sender, instance, created, *args, **kwargs):
    """
    Despues de creado, actualiza el valor total de la orden
    :param sender: sender
    :param instance: instance
    :param created: creado (boolean)
    :param args: args
    :param kwargs: kwargs
    """
    if not created:
        cart_obj = instance
        cart_total = cart_obj.total
        cart_id = cart_obj.id
        qs = Order.objects.filter(cart__id=cart_id)
        if qs.count() == 1:
            order_obj = qs.first()
            order_obj.update_total()


post_save.connect(post_save_cart_total, sender=Cart)


def post_save_order(sender, instance, created, *args, **kwargs):
    if created:
        instance.update_total()


post_save.connect(post_save_order, sender=Order)
