# coding=utf-8
from django.apps import AppConfig


class CartsConfig(AppConfig):
    name = 'carts'
