# coding=utf-8
from math import fsum
from django.db import models

from django.conf import settings
from products.models import Product
from django.db.models.signals import pre_save, m2m_changed

User = settings.AUTH_USER_MODEL


class CartManager(models.Manager):
    def new_or_get(self, request):
        """
        Revisa si el Cart ya existe.
        Si no existe, es creado
        :param request: request
        :return: objeto, creado
        """
        cart_id = request.session.get("cart_id", None)
        qs = self.get_queryset().filter(id=cart_id)
        if qs.count() == 1:
            created = False
            obj = qs.first()
            if request.user.is_authenticated() and obj.user is None:
                obj.user = request.user
                obj.save()
        else:
            obj = self.new(user=request.user)
            created = True
            request.session['cart_id'] = obj.id

        return obj, created

    def new(self, user=None):
        """
        Revisa si el usario es autentico
        :param user: usuario, sino None
        :return: objeto de cart del usuario
        """
        user_obj = None
        if user is not None:
            if user.is_authenticated():
                user_obj = user
        return self.model.objects.create(user=user_obj)


class Cart(models.Model):
    user        = models.ForeignKey(User, null=True, blank=True)
    products    = models.ManyToManyField(Product, blank=True)
    subtotal    = models.DecimalField(default=0.00, max_digits=100, decimal_places=2)
    total       = models.DecimalField(default=0.00, max_digits=100, decimal_places=2)
    iva         = models.DecimalField(default=0.00, max_digits=100, decimal_places=2)
    updated     = models.DateTimeField(auto_now=True)
    timestamp   = models.DateTimeField(auto_now_add=True)

    objects = CartManager()  # extiende las funciones de objects

    def __str__(self):
        return str(self.id)


def m2m_changed_cart_receiver(sender, instance, action, *args, **kwargs):
    """
    Suma los valores de los productos que son añadidos a la carreta
    :param sender: sender
    :param instance: instancia actual
    :param action: action
    :param args: args
    :param kwargs: kwargs
    """
    actions = ['post_add', 'post_remove', 'post_clear']
    if action in actions:
        products = instance.products.all()
        total = 0
        for x in products:
            total += x.price
        if instance.subtotal != total:
            instance.subtotal = format(total, '.2f')
            instance.save()


m2m_changed.connect(m2m_changed_cart_receiver, sender=Cart.products.through)


def pre_save_cart_receiver(sender, instance, *args, **kwargs):
    """
    Calcula el iva y lo añade a la suma total de la carreta
    :param sender: sender
    :param instance: instancia actual
    :param args: args
    :param kwargs: kwargs
    """
    if float(instance.subtotal) > 0:
        iva = float(instance.subtotal) * 0.12
        instance.iva = format(iva, '.2f')
        new_total = fsum([float(instance.subtotal), iva])
        instance.total = format(new_total, '.2f')
    else:
        instance.total = 0.00


pre_save.connect(pre_save_cart_receiver, sender=Cart)
