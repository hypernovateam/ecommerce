# coding=utf-8
from django.http import JsonResponse
from django.shortcuts import render, redirect

from accounts.forms import LoginForm, GuestForm
from addresses.forms import AddressForm
from addresses.models import Address
from billing.models import BillingProfile
from orders.models import Order
from .models import Cart
from products.models import Product


def cart_detail_api_view(request):
    cart_obj, new_obj = Cart.objects.new_or_get(request)
    products = [{
        "id": x.id,
        "url": x.get_absolute_url(),
        "title": x.title,
        "price": x.price
        }
        for x in cart_obj.products.all()]
    cart_data = {"products": products, "subtotal": cart_obj.subtotal, "iva": cart_obj.iva, "total": cart_obj.total}
    return JsonResponse(cart_data)

def cart_home(request):
    """
    Crea un objeto de tipo Cart donde se van a
    guardar los productos elegidos por el usuario
    :param request: request
    :return: objecto cart
    """
    cart_obj, new_obj = Cart.objects.new_or_get(request)
    return render(request, "carts/home.html", {'cart': cart_obj})


def cart_update(request):
    """
    Actualiza la carreta con los productos que el usario elige
    puede añadir o eliminar productos de la carreta
    :param request: request
    :return: producto añadido o eliminado y retorna a home
    """
    product_id = request.POST.get('product_id')

    if product_id is not None:
        try:
            product_obj = Product.objects.get(id=product_id)  # lo busca por id
        except Product.DoesNotExist:
            return redirect("cart:home")

        cart_obj, new_obj = Cart.objects.new_or_get(request)  # revisa si el producto ya esta o es nuevo

        if product_obj in cart_obj.products.all():
            cart_obj.products.remove(product_obj)  # si ya se encuentra en la carreta lo elimina
            added = False
        else:
            cart_obj.products.add(product_obj)  # si no se encuentra en la carreta lo añade
            added = True
        request.session['cart_items'] = cart_obj.products.count()  # suma la cantidad de productos en la carreta

        if request.is_ajax():  # Asynchronous JavaScript And XML
            json_data = {
                "added": added,
                "removed": not added,
                "cartItems": cart_obj.products.count()
            }
            return JsonResponse(json_data)
    return redirect("cart:home")


def checkout_home(request):
    """
    Obtiene todos los datos adquiridos del usuario
    profile, direccion de entrega y pago, y crea una orden
    :param request: request
    :return: total con los datos del usuario
    """
    cart_obj, cart_created = Cart.objects.new_or_get(request)
    order_obj = None
    if cart_created or cart_obj.products.count() == 0:
        return redirect("cart:home")

    login_form = LoginForm()  # en el checkout el usuario puede hacer login si ya creo una cuenta
    guest_form = GuestForm()  # el usuario puede hacer checkout como guest
    address_form = AddressForm()
    pago_address_id = request.session.get("pago_address_id", None)  # direccion actual del usuario
    entrega_address_id = request.session.get("entrega_address_id", None)  # direccion de entrega

    billing_profile, billing_profile_created = BillingProfile.objects.new_or_get(request)
    address_qs = None
    if billing_profile is not None:
        if request.user.is_authenticated():
            address_qs = Address.objects.filter(billing_profile=billing_profile)

        order_obj, order_obj_created = Order.objects.new_or_get(billing_profile, cart_obj)
        if entrega_address_id:
            order_obj.shipping_address = Address.objects.get(id=entrega_address_id)
            del request.session["entrega_address_id"]
        if pago_address_id:
            order_obj.billing_address = Address.objects.get(id=pago_address_id)
            del request.session["pago_address_id"]
        if entrega_address_id or pago_address_id:
            order_obj.save()

    # Si el usuario ya pago, la carreta se vacia y el orden cambia a pagado
    if request.method == "POST":
        if order_obj.mark_paid() == 'pagado':
            request.session['cart_items'] = 0
            del request.session['cart_id']
            return redirect("cart:success")

    context = {
        "object": order_obj,
        "billing_profile": billing_profile,
        "login_form": login_form,
        "guest_form": guest_form,
        "address_form": address_form,
        "address_qs": address_qs,
    }

    return render(request, "carts/checkout.html", context)


def checkout_done_view(request):
    """
    Finalizada la compra el usuario es dirigido a la pagina de terminado
    :param request: request
    :return: pagina de terminado
    """
    return render(request, "carts/checkout-done.html", {})
