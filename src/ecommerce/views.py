# coding=utf-8
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from .forms import ContactForm

from carts.models import Cart
from products.models import Product


def home_page(request):
    cart_obj, new_obj = Cart.objects.new_or_get(request)
    products = Product.objects.features()
    new_products = Product.objects.new_item()
    disc_products = Product.objects.discount_items()
    context = {
        'title': 'HyperNova',
        'content': 'El mejor eCommerce del Ecuador',
        'premium_content': 'yeah',
        'cart': cart_obj,
        'products': products,
        'newProducts': new_products,
        'discount': disc_products
    }
    if request.user.is_authenticated():
        context['premium_content'] = 'YEAHH'
    return render(request, 'home_page.html', context)


def about_page(request):
    context = {
        'title': 'About page',
        'content': 'Welcome to About',
    }
    return render(request, 'home_page.html', context)


def contact_page(request):
    contact_form = ContactForm(request.POST or None)
    context = {
        'title': 'Contactanos',
        'content': 'Dinos como te podemos ayudar!',
        'form': contact_form,
    }
    if contact_form.is_valid():
        if request.is_ajax():
            return JsonResponse({"message": "Gracias por contactarnos"})

    if contact_form.errors:
        errors = contact_form.errors.as_json()
        if request.is_ajax():
            return HttpResponse(errors, status=400, content_type='application/json')

    return render(request, 'contact/view.html', context)


