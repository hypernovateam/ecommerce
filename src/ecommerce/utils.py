# coding=utf-8
from django.utils.text import slugify
import random, string


def random_string_generator(size=10, chars=string.ascii_lowercase + string.digits):
    """
    Usado para generar nombres clave unicos
    :param size: tamaño de la palabra en letras o numeros
    :param chars: solo usa letras y numeros
    :return: el nombre clave unico
    """
    return ''.join(random.choice(chars) for _ in range(size))


def unique_order_id_generator(instance):
    """
    Crea el el numero de orden unico en la base de datos del servidor
    """

    new_order_id = random_string_generator().upper()

    Klass = instance.__class__
    qs_exists = Klass.objects.filter(order_id=new_order_id).exists()
    if qs_exists:
        return unique_order_id_generator(instance)
    return new_order_id


def unique_slug_generator(instance, new_slug=None):
    """
    Crea el nombre unico en la base de datos del servidor
    """
    if new_slug is not None:
        slug = new_slug
    else:
        slug = slugify(instance.title)

    Klass = instance.__class__
    qs_exists = Klass.objects.filter(slug=slug).exists()
    if qs_exists:
        new_slug = "{slug}-{randstr}".format(
            slug=slug,
            randstr=random_string_generator(size=4)
        )
        return unique_slug_generator(instance, new_slug=new_slug)
    return slug
