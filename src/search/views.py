# coding=utf-8
from django.views.generic import ListView
from products.models import Product


class SearchProductView(ListView):
    template_name = "search/view.html"

    def get_context_data(self, *args, **kwargs):
        context = super(SearchProductView, self).get_context_data(**kwargs)
        context['query'] = self.request.GET.get('q')
        return context

    def get_queryset(self, *args, **kwargs):
        """
        Funcion en clase de vista que muestra todos los productos
        :param args: datos
        :param kwargs: datos clave
        :return: todos los productos activos
        """
        request = self.request
        query = request.GET.get('q')
        if query is not None:
            return Product.objects.search(query)
        return Product.objects.features()
