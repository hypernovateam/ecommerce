# coding=utf-8
from django.db import models
from django.conf import settings
from django.db.models.signals import post_save

from accounts.models import GuestEmail
User = settings.AUTH_USER_MODEL


class BillingManager(models.Manager):
    def new_or_get(self, request):
        """
        Revisa si el profile del usuario ya existe
        Si no existe, un nuevo profile es creado
        :param request: request
        :return: objeto y boolean
        """
        user = request.user
        guest_email_id = request.session.get('guest_email_id')
        created = False
        obj = None
        if user.is_authenticated():
            # Checkout de usuario con sesion iniciada
            # Guarda las formas de pago
            obj, created = self.model.objects.get_or_create(user=user, email=user.email)

        elif guest_email_id is not None:
            # Checkout de usuario como guest
            # Envia a la forma de pago
            guest_email_obj = GuestEmail.objects.get(id=guest_email_id)
            obj, created = self.model.objects.get_or_create(email=guest_email_obj.email)
        else:
            pass

        return obj, created


class BillingProfile(models.Model):
    user        = models.OneToOneField(User, null=True, blank=True)
    email       = models.EmailField()
    active      = models.BooleanField(default=True)
    updated     = models.DateTimeField(auto_now=True)
    timestamp   = models.DateTimeField(auto_now_add=True)

    objects = BillingManager()  # extiende las funciones de objetos

    def __str__(self):
        return self.email


def user_created_receiver(sender, instance, created, *args, **kwargs):
    """
    si fue creado y con email valido, crea el profile
    :param sender: sender
    :param instance: instancia del momento
    :param created: boolean
    :param args: args
    :param kwargs: kwargs
    """
    if created and instance.email:
        BillingProfile.objects.get_or_create(user=instance, email=instance.email)


post_save.connect(user_created_receiver, sender=User)
