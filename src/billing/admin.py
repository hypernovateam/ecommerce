# coding=utf-8
from django.contrib import admin

from .models import BillingProfile

admin.site.register(BillingProfile)
