# coding=utf-8
from django.apps import AppConfig


class BillingConfig(AppConfig):
    name = 'billing'
