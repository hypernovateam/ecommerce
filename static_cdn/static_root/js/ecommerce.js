
$(document).ready(function() {
    // Cart toggle
    var shoppingCart = $(".shopping-cart");
    var cart = $("#cart");
    cart.on("click", function() {
        shoppingCart.fadeToggle("fast");
        shoppingCart.css("border", "1px solid #E8E8E8");
    });


    // Contact form
    var contactForm = $(".contact-form");
    var contactFormMethod = contactForm.attr("method");
    var contactFormEndpoint = contactForm.attr("action");


    function displaySubmitting(submitBtn, defaultText, doSubmit) {
        if (doSubmit) {
            contactFormBtn.addClass("disabled");
            contactFormBtn.html("<i class='fa fa-spin fa-spinner'></i> Enviando..");
        }
        else {
            contactFormBtn.removeClass("disabled");
            contactFormBtn.html(defaultText);
        }
    }


    contactForm.submit(function(event){
        event.preventDefault();
        var contactFormBtn = contactForm.find("[type='submit']");
        var contactFormBtnTxt = contactFormBtn.text();
        var contactFormData = contactForm.serialize();

        displaySubmitting(contactFormBtn, "", true);

        $.ajax({
            method: contactFormMethod,
            url: contactFormEndpoint,
            data: contactFormData,
            success: function(data){
                contactForm[0].reset();
                $.alert({
                    title: "Listo",
                    content: data.message,
                    theme: "modern"
                });
                setTimeout(function(){
                    displaySubmitting(contactFormBtn, contactFormBtnTxt, false)
                }, 500)
            },
            error: function(error){
                var jsonData = error.responseJSON;
                var msg = "";

                $.each(jsonData, function(key,value){
                    msg += key + ": " + value[0].message + "<br/>";
                });
                $.alert({
                    title: "Ooops",
                    content: msg,
                    theme: "modern"
                });
                setTimeout(function(){
                    displaySubmitting(contactFormBtn, contactFormBtnTxt, false)
                }, 500)
            }
        })
    });

    // Auto search
    var searchForm = $(".search-form");
    var searchInput = searchForm.find("[name='q']");
    var typingTimer;
    var typingInterval = 500;
    var searchBtn = searchForm.find("[type='submit']");

    searchInput.keyup(function (event) {
        // tecla despues de presionada
        clearTimeout(typingTimer);
        typingTimer = setTimeout(performSearch, typingInterval)
    });
    searchInput.keydown(function(event){
        // tecla presionada
        clearTimeout(typingTimer);

    });

    function displaySearching(){
        searchBtn.addClass("disabled");
        searchBtn.html("<i class='fa fa-spin fa-spinner'></i> Buscando..");
    }

    function performSearch(){
        displaySearching();
        var query = searchInput.val();
        setTimeout(function(){
            window.location.href='/search/?q=' + query;
        }, 1000)
    }



    // Relacionado con el form y Carreta
    var productForm = $(".form-product-ajax");

    productForm.submit(function (event) {
        event.preventDefault();
        var thisForm = $(this);
        var actionEndpoint = thisForm.attr("data-endpoint");
        var httpMethod = thisForm.attr("method");
        var formData = thisForm.serialize();

        $.ajax({
            url: actionEndpoint,
            method: httpMethod,
            data: formData,
            success: function (data) {
                var submitSpan = thisForm.find(".submit-span");
                if (data.added){
                    submitSpan.html("In cart <button type='submit' class='btn btn-link'>Remover?</button>");
                }
                else {
                    submitSpan.html("<button type='submit' class='btn btn-success'>Añadir</button>");
                }
                var navbarCount = $(".navbar-cart-count");
                navbarCount.text(data.cartItems);
                var currentPath = window.location.href;

                if (currentPath.indexOf("cart") !== -1){
                    refreshCart()
                }
            },
            error: function (errorData) {
                $.alert({
                    title: "Ooops",
                    content: "Ocurrio un error",
                    theme: "modern"
                })
            }
        })
    });

    function refreshCart(){
        var cartTable = $(".cart-table");
        var cartBody = cartTable.find(".cart-body");
        var productRows = cartBody.find(".cart-product");
        var currentURL = window.location.href;

        var refreshCartURL = "/api/cart/";
        var refreshCartMethod = "GET";
        var data = {};
        $.ajax({
            url: refreshCartURL,
            method: refreshCartMethod,
            data: data,
            success: function(data){
                var hiddenCartItemRemoveForm = $(".cart-item-remove-form");
                if (data.products.length > 0) {
                    productRows.html(" ");
                    i = data.products.length;
                    $.each(data.products, function(index, value){
                        var newCartItemRemove = hiddenCartItemRemoveForm.clone();
                        newCartItemRemove.css("display", "block");
                        newCartItemRemove.find(".cart-item-product-id").val(value.id);
                        cartBody.prepend("<tr><th scope='row'>" + i + "</th>" +
                            "<td><a href='" + value.url + "'>" + value.title + "</a>"
                            + newCartItemRemove.html() + "</td><td>"
                            + value.price + "</td></tr>");
                        i --;
                    });

                    cartBody.find(".cart-subtotal").text(data.subtotal);
                    cartBody.find(".cart-iva").text(data.iva);
                    cartBody.find(".cart-total").text(data.total);
                } else {
                    window.location.href = currentURL;
                }
            },
            error: function(errorData){
                $.alert({
                    title: "Ooops",
                    content: "Ocurrio un error",
                    theme: "modern"
                })
            }
        })
    }
});
